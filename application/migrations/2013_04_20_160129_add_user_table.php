<?php

class Add_User_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		DB::table('users')->insert(array(
				'email'=>'rezd14@gmail.com',
				'password'=>Hash::make('rahasia'),
				'level'=>'super admin'
			));
		DB::table('users')->insert(array(
				'email'=>'didiktrisusanto@gmail.com',
				'password'=>Hash::make('123456'),
				'level'=>'author'
			));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::table('users')->delete(1);
		DB::table('users')->delete(2);
	}

}