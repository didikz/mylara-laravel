<?php

class Add_Authors {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('authors')->insert(array(
				'name' => 'Didik Tri Susanto',
				'bio' => 'Didik is a great contributor at Bisakomputer',
				'created_at' => date('Y-m-d H:m'),
				'updated_at' => date('Y-m-d H:m')
			));

		DB::table('authors')->insert(array(
				'name' => 'Achmad mustofa',
				'bio' => 'Mustofa is CTO at mimicreative',
				'created_at' => date('Y-m-d H:m'),
				'updated_at' => date('Y-m-d H:m')
			));
		DB::table('authors')->insert(array(
				'name' => 'Maslicha Bisriyah',
				'bio' => 'she is an awesome techer',
				'created_at' => date('Y-m-d H:m'),
				'updated_at' => date('Y-m-d H:m')
			));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('authors')->delete(1);
		DB::table('authors')->delete(2);
		DB::table('authors')->delete(3);		
	}

}