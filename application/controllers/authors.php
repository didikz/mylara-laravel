<?php

class Authors_controller extends Base_Controller
{
	public $restful = true;
	
	public function get_index()
	{
		$view = View::make('authors.index')
					->with('title', 'my authors list')
					->with('authors', Authors::order_by('name')->get());
		return $view;
	}

	public function get_indexs()
	{

		return View::make('authors.indexs');
	}

	public function get_view($id)
	{
		$view = View::make('authors.view')
					->with('title', 'View author')
					->with('author', Authors::find($id));
		return $view;
	}

	public function get_new()
	{
		$view = View::make('authors.new')
				->with('title', 'add new author');
		return $view;
	}

	public function post_create()
	{
		//validate post input
		$validation = Authors::validate(Input::all());
		if($validation->fails())
		{

			return Redirect::to_route('new_author')->with_errors($validation)->with_input();

		} else{
			
			Authors::create(array(
				'name' => Input::get('name'),
				'bio' => Input::get('bio')
			));
			return Redirect::to('authors')->with('message', 'author successfully created');
		}
	}

	public function get_edit($id)
	{
		return View::make('authors.edit')
					->with('title', 'Edit author')
					->with('author', Authors::find($id));
	}

	public function post_update()
	{
		$id = Input::get('id');
		$validation = Authors::validate(Input::all());
		if($validation->fails())
		{
			return Redirect::to_route('edit_author', $id)->with_errors($validation);
		}	
		else
		{
			Authors::update($id, array(
				'name' => Input::get('name'),
				'bio' => Input::get('bio')
			));
			return Redirect::to_route('author', $id)->with('message', 'author updated successfully');
		}
	}

	public function delete_delete()
	{
		Authors::find(Input::get('id'))->delete();
		return Redirect::to_route('authors')->with('message', 'Author successfully deleted');
	}
}