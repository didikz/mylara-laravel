@layout('layouts.default')

@section('content')
	<h1>Login Page</h1>

	{{ Form::open('users/login', 'POST') }}
	{{ Form::token() }}
		<p>
			{{ Form::label('email', 'email') }}
			{{ Form::text('email', Input::old('email'))}}
		</p>	
		<p>
			{{ Form::label('password', 'password') }}
			{{ Form::password('password') }}
		</p>
		<p>
			{{ Form::label('level', 'level') }}
			{{ Form::select('level', array('super admin'=>'super admin','author'=>'author')) }}
		</p>
		<p>
			{{ Form::submit('Login') }}
		</p>
	{{ Form::close()}}

@endsection