@layout('layouts.default')

@section('content')
	<h1>Editing {{ e($author->name) }}</h1>
	
	{{ render('common.author_error')}}

	{{ Form::open('author/update', 'POST') }}

	{{ Form::token() }}

	<p>
		{{ Form::label('name', 'Name:') }}<br />
		{{ Form::text('name', $author->name) }}
	</p>

	<p>
		{{ Form::label('bio', 'Biography:') }}<br />
		{{ Form::textarea('bio', $author->bio) }}
	</p>
		{{ Form::hidden('id', $author->id) }}
	<p>{{ Form::submit('update author') }} </p>

	{{ Form::close() }}
@endsection	