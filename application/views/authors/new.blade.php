@layout('layouts.default')

@section('content')
	<h1>New author</h1>

	{{ render('common.author_error')}}

	{{ Form::open('authors/create', 'POST') }}

	{{ Form::token() }}
	<p>
		{{ Form::label('name', 'Name:') }}<br />
		{{ Form::text('name', Input::old('name')) }}
	</p>

	<p>
		{{ Form::label('bio', 'Biography:') }}<br />
		{{ Form::textarea('bio', Input::old('bio')) }}
	</p>

	<p>{{ Form::submit('Add author') }} </p>

	{{ Form::close() }}

@endsection	