@layout('layouts.default')

@section('content')
	<h1>This is Authors home page</h1>

	<ul>
	@foreach($authors as $author)
		<li>{{ HTML::link_to_route('author', $author->name, array($author->id)) }}</li>
	@endforeach
	</ul>

	{{ HTML::link_to_route('new_author', 'New Author')}}
@endsection	